/*!
\file saisieEntier.c
\autor Jalbert Sylvain
\version 1
\date 17 octobre 2019
\brief un programme qui ecrit un entier saisie
*/

#include <stdio.h>
#include <stdlib.h>

/*!
\def ERREUR SAISIE
Code d’erreur associe a la saisie
*/
#define ERREUR_SAISIE -1

/*!
\fn int saisieEntier ( void )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 octobre 2019
\brief une fonction qui va demander la saisie d'un nombre entier, elle va verifier tant que le nombre n'est pas un entier, puis elle va le retourner
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return un entier
*/
int saisieEntier (void){
  /*DECLARATION DES VARIABLES*/
  int int_n; //le nombre qui sera saisie

  /*SAISIE DE L'ENTIER*/
  printf("Veuillez saisir un entier:\n"); //Demmande de saisie
  if(!scanf("%d",&int_n)){ //saisie du nombre, si ce n'est pas un entier, alors on quite le programme
    printf("Erreur de saisie : La saisie n'est pas un entier\n"); //message indiquant l'erreur
    exit(ERREUR_SAISIE); //on quite le programme
  }
  else{ //Si le nombre saisie est un entier, alors on retourne sa valeur
    return int_n;
  }
}

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 octobre 2019
\brief la fonction principale sui va appeler la fonction saisieNombre et afficher ce qu'elle retourne
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  /*DECLARATION DES VARIABLES*/
  int int_nb; //l'entier qui sera saisie

  /*SAISIE DE L'ENTIER*/
  int_nb = saisieEntier();

  /*AFFICHAGE DE L'ENTIER*/
  printf("%d\n", int_nb);

  /* Fin du programme, Il se termine normalement, et donc retourne 0 */
  return 0;
}
