/*!
\file porteeDUneVariable.c
\autor Jalbert Sylvain
\version 1
\date 17 octobre 2019
\brief un programme qui permet de voir la portée d'une variable
*/

#include <stdio.h>
#include <stdlib.h>

/*!
\fn void bidon ( void )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 octobre 2019
\brief une fonction qui va demander la saisie d'un nombre entier et l'afficher
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return un entier
*/
void bidon (void){
  //DECLARATION DES VARIABLES
  int int_n; //le nombre qui sera saisie

  //AFFICHAGE DE L'ENTIER
  printf("Avant saisie, l'entier dans la procédure bidon est: %d\n", int_n); //affichage de int_n, avec un message pour bien comprendre où l'on se situe !

  //SAISIE DE L'ENTIER
  printf("Veuillez saisir un entier: "); //Demmande de saisie
  scanf("%d",&int_n); //saisie du nombre

  //AFFICHAGE DE L'ENTIER
  printf("Après saisie, l'entier dans la procédure bidon est: %d\n", int_n); //affichage de int_n, avec un message pour bien comprendre où l'on se situe !
}

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 10 octobre 2019
\brief la fonction principale sui va afficher un entier avant et après saisie, puis qui va appeler la fonction bidon et va ensuit re-afficher l'entier
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  //DECLARATION DES VARIABLES
  int int_n; //le nombre qui sera saisie

  //AFFICHAGE DE L'ENTIER
  printf("Avant saisie, l'entier dans la fontion principale est: %d\n", int_n); //affichage de int_n, avec un message pour bien comprendre où l'on se situe !

  //SAISIE DE L'ENTIER
  printf("Veuillez saisir un entier: "); //Demmande de saisie
  scanf("%d",&int_n); //saisie du nombre

  //AFFICHAGE DE L'ENTIER
  printf("Après saisie, l'entier dans la fontion principale est: %d\n", int_n); //affichage de int_n, avec un message pour bien comprendre où l'on se situe !

  //APPEL DE LA PROCEDURE bidon
  bidon();

  //AFFICHAGE DE L'ENTIER
  printf("Après appel de bidon, l'entier dans la fontion principale est: %d\n", int_n); //affichage de int_n, avec un message pour bien comprendre où l'on se situe !

  /* Fin du programme, Il se termine normalement, et donc retourne 0 */
  return 0;
}
