#Readme.md
Ce fichier readme.md contient les réponses des exercices du TP ainsi que les instructions pour compiler et executer les programmes du TP.

##Documentation
La documentation est faite avec doxygen. Pour l'observée executer la comande suivante :
`doxygen Doxyfile`
Puis visualiser dans votre navigateur le fichier ./html/index.html

##Exercice 1 : Saisie d'entier
###Compiler
Pour compiler, il faut saisir dans le terminal linux, à la racine du projet, la commande suivante :
`gcc ./saisieEntier.c -o ./saisieEntier`

###Executer
Pour executer, il suffi de saisir dans le terminal linux, à la racine du projet, la commande suivante :
`./saisieEntier`

##Exercice 2 : Portée d'une variable
###Compiler
Pour compiler, il faut saisir dans le terminal linux, à la racine du projet, la commande suivante :
`gcc ./porteeDUneVariable.c -o ./porteeDUneVariable`

###Executer
Pour executer, il suffi de saisir dans le terminal linux, à la racine du projet, la commande suivante :
`./porteeDUneVariable`
